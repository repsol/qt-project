#include <QtWidgets>
#include <QtNetwork>
#include "client.h"

Client::Client(QWidget *parent)
    : QDialog(parent), networkSession(0)
{
    hostLabel = new QLabel(tr("&Server name:"));
    portLabel = new QLabel(tr("S&erver port:"));

    // find out which IP to connect to
    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

    //hostLineEdit = new QLineEdit(ipAddress);
    hostLineEdit = new QLineEdit("127.0.0.1");
    portLineEdit = new QLineEdit("55555");
    portLineEdit->setValidator(new QIntValidator(1, 65535, this));

    hostLabel->setBuddy(hostLineEdit);
    portLabel->setBuddy(portLineEdit);

    statusLabel = new QLabel("Client app for testing Server.");
    numberLabel = new QLabel("number: ");

    quitButton = new QPushButton(tr("Quit"));
    connectButton = new QPushButton(tr("Connect"));
    connectButton->setDefault(true);
    connectButton->setEnabled(false);

    //Chat section
    nickLabel = new QLabel("&Nick: ");
    nickLineEdit = new QLineEdit;
    nickLabel->setBuddy(nickLineEdit);
    chatBox = new QTextEdit;
    chatBox->setReadOnly(true);
    messageLineEdit =  new QLineEdit;
    messageLineEdit->setEnabled(false);
    sendButton = new QPushButton("Send");
    sendButton->setEnabled(false);
    //Chat end

    tcpSocket = new QTcpSocket(this);

    connect(hostLineEdit, SIGNAL(textChanged(QString)), this, SLOT(enableConnectButton()));
    connect(portLineEdit, SIGNAL(textChanged(QString)), this, SLOT(enableConnectButton()));
    connect(connectButton, SIGNAL(clicked()), this, SLOT(connectToServer()));
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(getNewData()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));
    connect(sendButton, SIGNAL(clicked()), this, SLOT(sendNewData()));
    connect(messageLineEdit, SIGNAL(returnPressed()), this, SLOT(sendNewData()));

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(hostLabel, 0, 0);
    mainLayout->addWidget(hostLineEdit, 0, 1);
    mainLayout->addWidget(portLabel, 1, 0);
    mainLayout->addWidget(portLineEdit, 1, 1);
    mainLayout->addWidget(statusLabel, 2, 0, 1, 2);
    mainLayout->addWidget(numberLabel, 3, 0, 1, 2);
    mainLayout->addWidget(connectButton, 4, 0);
    mainLayout->addWidget(quitButton, 4, 1);
    mainLayout->addWidget(nickLabel, 5, 0);
    mainLayout->addWidget(nickLineEdit, 5, 1);
    mainLayout->addWidget(chatBox, 6, 0, 2, 2);
    mainLayout->addWidget(messageLineEdit, 8, 0);
    mainLayout->addWidget(sendButton, 8, 1);
    setLayout(mainLayout);

    setWindowTitle(tr("Client"));
    portLineEdit->setFocus();

    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {

        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = manager.defaultConfiguration();

        networkSession = new QNetworkSession(config, this);
        connect(networkSession, SIGNAL(opened()), this, SLOT(sessionOpened()));

        //connectButton->setEnabled(false);
        statusLabel->setText(tr("Opening network session."));
        networkSession->open();
    }

    enableConnectButton();
}

Client::~Client()
{
    tcpSocket->abort();
}

void Client::connectToServer()
{
    if (nickLineEdit->text().isEmpty())
    {
        QMessageBox::warning(this, "Warrning", "Nick cannot be empty.");
        return;
    }

    connectButton->setEnabled(false);
    nickLineEdit->setReadOnly(true);
    sendButton->setEnabled(true);
    messageLineEdit->setEnabled(true);
    tcpSocket->abort();
    tcpSocket->connectToHost(hostLineEdit->text(), portLineEdit->text().toInt());
}

void Client::getNewData()
{
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);

    quint16 blockSize=0;
    if (blockSize == 0) {
        if (tcpSocket->bytesAvailable() < (int)sizeof(quint16))
            return;

        in >> blockSize;
    }

    if (tcpSocket->bytesAvailable() < blockSize)
        return;

    qint16 type;
    in >> type;

    if (type == 1)  //new number
    {
        int newNumber;
        in >> newNumber;
        numberLabel->setText("Number: "+QString::number(newNumber));
    }
    else if (type == 2)  //new mesage
    {
        QString newMessage;
        in >> newMessage;
        chatBox->append(QTime::currentTime().toString("H:mm:ss")+" "+newMessage);
    }
    else  //unknow type
    {
        QMessageBox::warning(this, "Warrning", "Unknow type of transmission.");
    }
}

void Client::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The host was not found. Please check the "
                                    "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The connection was refused by the peer. "
                                    "Make sure the Server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Client"),
                                 tr("The following error occurred: %1.")
                                 .arg(tcpSocket->errorString()));
    }

    connectButton->setEnabled(true);
    sendButton->setEnabled(false);
    messageLineEdit->setEnabled(false);
}
//! [13]

void Client::enableConnectButton()
{
    connectButton->setEnabled((!networkSession || networkSession->isOpen()) &&
                                 !hostLineEdit->text().isEmpty() &&
                                 !portLineEdit->text().isEmpty());

}

void Client::sessionOpened()
{
    statusLabel->setText(tr("This examples requires that you run the "
                            "Server example as well."));

    enableConnectButton();
}

void Client::sendNewData()
{
    //send some data to server
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    QString newMessage = messageLineEdit->text();
    out << (quint16)0 << (qint16)2; //(qint16)2 means message
    out << nickLineEdit->text()+": "+newMessage;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    if (tcpSocket && tcpSocket->isOpen() && tcpSocket->isWritable())
        tcpSocket->write(block);

    messageLineEdit->clear();
}
