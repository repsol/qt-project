#ifndef CLIENT_H
#define CLIENT_H

#include <QtWidgets>
#include <QtNetwork>

class Client : public QDialog
{
    Q_OBJECT
    
public:
    Client(QWidget *parent = 0);
    ~Client();

private slots:
    void getNewData();
    void sendNewData();
    void displayError(QAbstractSocket::SocketError socketError);
    void enableConnectButton();
    void connectToServer();
    void sessionOpened();

private:
    QLabel *hostLabel, *portLabel;
    QLabel *nickLabel;
    QLineEdit *hostLineEdit, *portLineEdit;
    QLineEdit *nickLineEdit, *messageLineEdit;
    QTextEdit *chatBox;
    QLabel *statusLabel;
    QLabel *numberLabel;
    QPushButton *quitButton, *connectButton;
    QPushButton *sendButton;
    QTcpSocket *tcpSocket;
    QNetworkSession *networkSession;
};

#endif // CLIENT_H
