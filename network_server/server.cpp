#include <QtWidgets>
#include <QtNetwork>
#include "server.h"

Server::Server(QWidget *parent)
    : QDialog(parent), tcpServer(0), networkSession(0)
{
    label = new QLabel;
    labelCount = new QLabel;
    labelInfo = new QLabel;
    button = new QPushButton("Quit");
    socketList = new QList<QTcpSocket*>;

    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired)
    {
        QNetworkConfiguration config = manager.defaultConfiguration();
        networkSession = new QNetworkSession(config, this);
        connect(networkSession, SIGNAL(opened()), this, SLOT(sessionOpened()));
        label->setText("Opening network session.");
        networkSession->open();  //mozna zapisac parametry do pliku
    }
    else
        sessionOpened();

    connect(button, SIGNAL(clicked()), this, SLOT(close()));
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newConnection()));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(label);
    mainLayout->addWidget(labelCount);
    mainLayout->addWidget(labelInfo);
    mainLayout->addWidget(button);
    setLayout(mainLayout);

    setWindowTitle("Server");

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);
}

Server::~Server()
{

    // at end
    delete socketList;
}

void Server::sessionOpened()
{
    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::Any, 55555)) {
        QMessageBox::critical(this, "Server",
                              tr("Unable to start the server: %1.")
                              .arg(tcpServer->errorString()));
        close();
        return;
    }

    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

    label->setText(tr("The server is running on\n\nIP: %1\nport: %2\n\n"
                            "Run the Client now.")
                         .arg(ipAddress).arg(tcpServer->serverPort()));
    labelCount->setText("Number of connections: 0");
}

void Server::newConnection()
{
    QTcpSocket *clientConnection = (tcpServer->nextPendingConnection());
    connect(clientConnection, SIGNAL(disconnected()), this, SLOT(clearList()));
    connect(clientConnection, SIGNAL(readyRead()), this, SLOT(getNewData()));

    socketList->push_back(clientConnection);
    labelCount->setText("Number of connections: "+QString::number(socketList->count()));
}

void Server::clearList()
{
    QListIterator<QTcpSocket*> iter(*socketList);
    QTcpSocket *socket;
    while(iter.hasNext())
    {
        socket = iter.next();
        if (socket->state() == QAbstractSocket::UnconnectedState)
        {
            socketList->removeOne(socket);  //remove address from list
            labelCount->setText("Number of connections: "+QString::number(socketList->count()));
        }
    }
}

void Server::update()
{
    if (socketList->isEmpty())
        return;

    int newNumber;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    newNumber = (qrand() % 1000);
    out << (quint16)0 << (qint16)1; //(qint16)1 means number
    out << newNumber;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    QListIterator<QTcpSocket*> iter(*socketList);
    QTcpSocket *socket;
    while(iter.hasNext())
    {
        socket = iter.next();
        if (socket && socket->isOpen() && socket->isWritable())
            socket->write(block);
    }

//    QTcpSocket *clientConnection;
//    for (int i=0; i < socketList->count(); ++i) //zrobic na iteratorze
//    {
//        clientConnection = socketList->at(i);
//        if (clientConnection && clientConnection->isOpen() && clientConnection->isWritable())
//            clientConnection->write(block);
//    }

    labelInfo->setText("Number: "+QString::number(newNumber)+"\nsended to "+
                       QString::number(socketList->count())+" clients.");
}

void Server::getNewData()
{
    //get data from one client and send it to all others

    //looking for socket
    QListIterator<QTcpSocket*> iter(*socketList);
    QTcpSocket *socket;
    while(iter.hasNext())
    {
        socket = iter.next();
        if (socket && socket->bytesAvailable())
            break;
    }

    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_4_0);

    if (socket->bytesAvailable() < (int)sizeof(quint16))
        return;

    qint16 size;
    in >> size;

    if (socket->bytesAvailable() < size)
        return;

    qint16 type;
    in >> type;

    if (type != 2) //not message
        return;

    QString newMessage;
    in >> newMessage;
//    QMessageBox::information(this, "new message", newMessage);

    // send newMessage to all Clients

    if (socketList->isEmpty())
        return;

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    out << (quint16)0 << (qint16)2; //(qint16)2 means message
    out << newMessage;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    QTcpSocket *clientConnection;
    for (int i=0; i < socketList->count(); ++i) //zrobic na iteratorze
    {
        clientConnection = socketList->at(i);
        if (clientConnection && clientConnection->isOpen() && clientConnection->isWritable())
            clientConnection->write(block);
    }
}
