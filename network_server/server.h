#ifndef SERVER_H
#define SERVER_H

#include <QtWidgets>
#include <QtNetwork>

class Server : public QDialog
{
    Q_OBJECT
    
public:
    Server(QWidget *parent = 0);
    ~Server();

private slots:
    void sessionOpened();
    void newConnection();
    void clearList();
    void update();
    //void getNewData(QTcpSocket *socket);
    void getNewData();

private:
    QLabel *label, *labelCount, *labelInfo;
    QPushButton *button;
    QTcpServer *tcpServer;
    QNetworkSession *networkSession;
    QList<QTcpSocket*> *socketList; // list on Clients
    QTimer *timer; //periodic update
};

#endif // SERVER_H
