#-------------------------------------------------
#
# Project created by QtCreator 2012-11-15T22:00:02
#
#-------------------------------------------------

QT       += core network widgets

TARGET = network_server
TEMPLATE = app


SOURCES += main.cpp\
        server.cpp

HEADERS  += server.h
